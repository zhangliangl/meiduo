import re

from django.contrib.auth.backends import ModelBackend

from users.models import User


def jwt_response_payload_handler(token, user=None, request=None):
    """自定义jwt认证成功返回"""

    return {
        'token': token,
        'user_id': user.id,
        'username':user.username
    }

# 判断用户输入的是电话还是手机号
def get_user_by_accoutn(account):
    try:
        if re.match(r'^1[3-9]\d{9}$', account):
            user = User.objects.get(mobile=account)
        else:
            user = User.objects.get(username=account)
    except User.DoesNotExist:
        return None
    else:
        return user
    

class UsernameMobileAuthBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        """
        自定义用户名或手机号认证
        """
        user = get_user_by_accoutn(username)
        return user

