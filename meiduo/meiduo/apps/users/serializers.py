import re

from django_redis import get_redis_connection
from rest_framework import serializers

from celery_tasks.email.tasks import send_email
from goods.models import SKU
from users import constants
from users.models import User, Address


class CreateUserSerializer(serializers.ModelSerializer):
    """用户注册序列化器类"""
    password2 = serializers.CharField(label='重复密码', write_only=True)
    sms_code = serializers.CharField(label='短信验证码', write_only=True)
    allow = serializers.CharField(label='同意协议', write_only=True)
    token = serializers.CharField(label='jwt token', read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'password2', 'mobile', 'sms_code', 'allow', 'token')

        extra_kwargs = {
            'username': {
                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }

    def validate_mobile(self, value):
        """手机号格式，手机号是否注册"""
        # 手机号格式
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式不正确')

        # 手机号是否注册
        res = User.objects.filter(mobile=value).count()

        if res > 0:
            raise serializers.ValidationError('手机号已注册')

        return value

    def validate_allow(self, value):
        """是否同意协议"""
        # 是否同意协议
        if value != 'true':
            raise serializers.ValidationError('请同意协议')

        return value

    def validate(self, attrs):
        """两次密码是否一致，短信验证码是否正确"""
        # 两次密码是否一致
        password = attrs['password']
        password2 = attrs['password2']

        if password != password2:
            raise serializers.ValidationError('两次密码不一致')

        # 短信验证码是否正确
        mobile = attrs['mobile']

        # 从redis中获取真实的短信验证码文本
        redis_conn = get_redis_connection('verify_codes')
        # bytes
        real_sms_code = redis_conn.get('sms_code_%s' % mobile)

        if real_sms_code is None:
            raise serializers.ValidationError('短信验证码已过期')

        # 对比短信验证码
        sms_code = attrs['sms_code'] # str

        if sms_code != real_sms_code.decode():
            raise serializers.ValidationError('短信验证码错误')

        return attrs

    def create(self, validated_data):
        """创建新用户并保存注册用户的信息"""
        # 清除无用的数据
        del validated_data['password2']
        del validated_data['sms_code']
        del validated_data['allow']

        # 创建新用户并保存注册用户的信息
        user = User.objects.create_user(**validated_data)

        # # 由服务器生成一个jwt token，保存当前用户的身份信息
        from rest_framework_jwt.settings import api_settings

        # 组织payload数据的方法
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        # 生成jwt token数据的方法
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        # 组织payload数据
        payload = jwt_payload_handler(user)
        # 生成jwt token
        token = jwt_encode_handler(payload)
        #
        # # 给user对象增加属性token，保存生成jwt token数据
        user.token = token

        # 返回user
        return user


class UserDetailSerializer(serializers.ModelSerializer):
    """
    个人信息序列化器
    """
    class Meta:
        model = User
        fields = ('id', 'username', 'mobile', 'email', 'email_active')
        
class EmailSerializer(serializers.ModelSerializer):
    """邮箱校验并发送邮件"""
    class Meta:
        model = User
        fields = ('id', 'email')
        
    def update(self, instance, validated_data):
        email = validated_data['email']
        instance.email = email
        instance.save()
        verify_url = instance.generic_verify_email_url()
        # 发送邮件给用户邮箱
        # send_email(email, verify_url)
        # 异步发送邮件
        send_email.delay(email, verify_url)
        return instance
    



class UserAddressSerializer(serializers.ModelSerializer):
    """
    用户地址序列化器
    """
    province = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)
    district = serializers.StringRelatedField(read_only=True)
    province_id = serializers.IntegerField(label='省ID', required=True)
    city_id = serializers.IntegerField(label='市ID', required=True)
    district_id = serializers.IntegerField(label='区ID', required=True)

    class Meta:
        model = Address
        exclude = ('user', 'is_deleted', 'create_time', 'update_time')

    def validate_mobile(self, value):
        """
        验证手机号
        """
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式错误')
        return value

    def create(self, validated_data):
        """
        保存
        """
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)


class AddressTitleSerializer(serializers.ModelSerializer):
    """
    地址标题
    """
    class Meta:
        model = Address
        fields = ('title',)
        

class BrowsingHistorySerializer(serializers.Serializer):
    
    """历史记录序列化器"""
    sku_id = serializers.IntegerField(label='商品', min_value=1)
    
    def validate_sku_id(self, value):
        """检验sku_id是否存在"""
        
        try:
            SKU.objects.get(id=value)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('商品不存在')
        return value
    
    def create(self, validated_data):
        """保存"""
        
        user_id = self.context['request'].user.id
        sku_id = validated_data['sku_id']
        
        # 获取redis数据库连接
        redis_con = get_redis_connection('history')
        pl= redis_con.pipeline()
        
        # 先删除已存在的重复的记录（因为要顺序存储）
        pl.lrem('history_%s' %user_id, 0, sku_id)
        #添加记录
        pl.lpush('history_%s' %user_id, sku_id)
        #设置保存记录条数（使用ltrim剪切去除多余的记录）
        # pl.ltrim(键，开始位置，结束位置)
        pl.ltrim('history_%s' %user_id, 0,constants.USER_BROWSING_HISTORY_COUNTS_LIMIT-1 )
        
        pl.execute()
        
        return validated_data


        
        











