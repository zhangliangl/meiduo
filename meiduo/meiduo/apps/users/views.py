from django.shortcuts import render
# Create your views here.
from django_redis import get_redis_connection
from rest_framework import mixins
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from carts.utils import merge_cart_cookie_to_redis
from goods.models import SKU
from users import constants
from users import serializers
from users.models import User


# 验证用户名是否存在
# usernames/(?P<username>\w{5,20})/count/
from users.serializers import CreateUserSerializer
from goods.serializers import SKUSerializer
from rest_framework_jwt.views import ObtainJSONWebToken


class UsernameCountView(APIView):
    """用户名是否存在"""
    def get(self, request, username):
        
        count = User.objects.filter(username=username).count()
        
        data = {
            'username' : username,
            'count' : count
        }
        return Response(data)


# mobile/(?P<mobile>1[3-9]\d{9})/count/

class MobileCountView(APIView):
    """验证手机号是否存在"""
    def get(self, request, mobile):
        # 获取手机号
        print('mobile %s' % mobile)
        count = User.objects.filter(mobile=mobile).count()
        data = {
            'mobile': mobile,
            'count': count,
        }
        return Response(data)


from rest_framework.permissions import IsAuthenticated


# url(r'^users/$', views.UserView.as_view()),
# class UserView(GenericAPIView,CreateModelMixin):
class UserView(CreateAPIView):
    """注册"""
    serializer_class = CreateUserSerializer
    


# class UserDetailView(GenericAPIView):
class UserDetailView(RetrieveAPIView):
    """个人信息"""
    permission_classes = [IsAuthenticated]      #用户必须登陆
    serializer_class = serializers.UserDetailSerializer
    def get_object(self):
        return self.request.user
    # def get(self, request):
    #
    #     # 获取用户信息(用户通过验证，request.user为当前用户)
    #     user = request.user
    #     # 将用户信息序列化并返回
    #     serializer = self.get_serializer(user)
    #     return Response(serializer.data, status=status.HTTP_200_OK)

class EmailView(GenericAPIView):
    """设置邮箱"""
    permission_classes = [IsAuthenticated]      #用户必须登陆
    serializer_class = serializers.EmailSerializer
    def get_object(self):
        return self.request.user
        
    def put(self,request):
        # 获取当前用户
        user = self.get_object()
        print(request.data)
        serializer = self.get_serializer(user,request.data)
        print(serializer.is_valid())
        serializer.is_valid()
        serializer.save()
        
        return Response(serializer.data)
    

class VerifyEmailView(APIView):
    """邮箱激活验证"""
    def put(self, request):
        token = request.query_params.get('token')
        if not token:
            return Response({"message:缺失参数"},status=status.HTTP_400_BAD_REQUEST)
        user = User.check_verify_email_token(token)
        if user is None:
            return Response({'message:token失效'}, status=status.HTTP_400_BAD_REQUEST)
        # 设置对应用户的邮箱验证标记email_active
        user.email_active = True
        user.save()

        return Response({'message':'邮箱激活成功'},status=status.HTTP_200_OK)

class AddressViewSet(mixins.CreateModelMixin, mixins.UpdateModelMixin, GenericViewSet):
    """
    用户地址新增与修改
    """
    serializer_class = serializers.UserAddressSerializer
    permissions = [IsAuthenticated]

    def get_queryset(self):
        return self.request.user.addresses.filter(is_deleted=False)

    # GET /addresses/
    def list(self, request, *args, **kwargs):
        """
        用户地址列表数据
        """
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        user = self.request.user
        return Response({
            'user_id': user.id,
            'default_address_id': user.default_address_id,
            'limit': constants.USER_ADDRESS_COUNTS_LIMIT,
            'addresses': serializer.data,
        })

    # POST /addresses/
    def create(self, request, *args, **kwargs):
        """
        保存用户地址数据
        """
        # 检查用户地址数据数目不能超过上限
        count = request.user.addresses.filter(is_deleted=False).count()
        if count >= constants.USER_ADDRESS_COUNTS_LIMIT:
            return Response({'message': '保存地址数据已达到上限'}, status=status.HTTP_400_BAD_REQUEST)
    
        return super().create(request, *args, **kwargs)

    # delete /addresses/<pk>/
    def destroy(self, request, *args, **kwargs):
        """
        处理删除
        """
        address = self.get_object()
    
        # 进行逻辑删除
        address.is_deleted = True
        address.save()
    
        return Response(status=status.HTTP_204_NO_CONTENT)

    # put /addresses/pk/status/
    @action(methods=['put'], detail=True)
    def status(self, request, pk=None):
        """
        设置默认地址
        """
        address = self.get_object()
        request.user.default_address = address
        request.user.save()
        return Response({'message': 'OK'}, status=status.HTTP_200_OK)

    # put /addresses/pk/title/
    # 需要请求体参数 title
    @action(methods=['put'], detail=True)
    def title(self, request, pk=None):
        """
        修改标题
        """
        address = self.get_object()
        serializer = serializers.AddressTitleSerializer(instance=address, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
    
class BrowingHistoryView(GenericAPIView):
    serializer_class = serializers.BrowsingHistorySerializer
   
    # 添加历史信息
    def post(self,request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    # 获取历史信息
    def get(self, request):
        # 获取用户信息
        user_id = request.user.id

        redis_con = get_redis_connection('history')
        history = redis_con.lrange('history_%s' %user_id, 0, -1)
        skus = []
        # 保持查询顺序与用户浏览顺序一致
        for sku_id in history:
            sku = SKU.objects.get(id=sku_id)
            skus.append(sku)
            
        serializer = SKUSerializer(skus,many=True)
        print(serializer.data)
        return Response(serializer.data)
    
class UserAuthorizeView(ObtainJSONWebToken):
    """
    用户认证
    """
    def post(self, request, *args, **kwargs):
        # 调用父类的方法，获取drf jwt扩展默认的认证用户处理结果
        response = super().post(request, *args, **kwargs)

        # 仿照drf jwt扩展对于用户登录的认证方式，判断用户是否认证登录成功
        # 如果用户登录认证成功，则合并购物车
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = serializer.validated_data.get('user')
            response = merge_cart_cookie_to_redis(request, user, response)

        return response

    