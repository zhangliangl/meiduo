from django.shortcuts import render
from drf_haystack.viewsets import HaystackViewSet
from rest_framework.filters import OrderingFilter
from rest_framework.generics import  GenericAPIView, ListAPIView
# Create your views here.
from rest_framework.response import Response

from goods.models import SKU
from goods.serializers import SKUSerializer, SKUIndexSerializer
from meiduo.utils.pagination import StandardResultPagination


# class SKUListView(GenericAPIView):
class SKUListView(ListAPIView):
    """商品列表"""
    serializer_class = SKUSerializer
    # 注意:restfromwork的分页功能只有在继承ListModelMixin才生效
    pagination_class = StandardResultPagination
    filter_backends = [OrderingFilter]
    ordering_fields = ('create_time', 'price', 'sales')
    def get_queryset(self):
        category_id = self.kwargs['category_id']
        return SKU.objects.filter(category_id=category_id,is_launched=True)

    # def get(self,request,category_id):
    #     skus = self.get_queryset()
    #
    #     serializer = self.get_serializer(skus,many=True)
    #     return Response(serializer.data)

# /skus/search/?text=<关键字>
class SKUSearchViewSet(HaystackViewSet):
    """SKU搜索"""
    index_models = [SKU]
    serializer_class = SKUIndexSerializer




    
       
            
        
        
        