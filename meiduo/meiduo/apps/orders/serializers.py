from rest_framework import serializers

from goods.models import SKU


class CartSKUSerializer(serializers.ModelSerializer):
    """订单信息序列化"""
    count = serializers.IntegerField(label='商品总数量')
    class Meta:
        model = SKU
        fields = ('id', 'name', 'price', 'default_image_url','count')

class OrderSettlementSerializer(serializers.Serializer):
    """
    订单结算数据序列化器
    """
    freight = serializers.DecimalField(label='运费', max_digits=10, decimal_places=2)
    skus = CartSKUSerializer(many=True)
    
