from decimal import Decimal
from django.shortcuts import render

# Create your views here.


# GET /orders/settlement/
from django_redis import get_redis_connection
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from goods.models import SKU
from orders.serializers import OrderSettlementSerializer


class OrderSettlementView(APIView):
    
    """
    订单信息
    1.从redis中查询购物车中勾选状态的商品
    ２.从mysql中获取对应商品信息
    组织数据并返回
    """
    permission_classes = [IsAuthenticated]
    def get(self, request):
        user = request.user
        # 1.从redis中查询购物车中勾选状态的商品
        redis_con = get_redis_connection('cart')
        redis_cart = redis_con.hgetall('cart_%s' % user.id)
        cart_selected = redis_con.smembers('cart_selected_%s' % user.id)
        
        # 获取勾选商品数量（字典）
        cart = {}
        for sku_id in cart_selected:
            cart[int(sku_id)] = int(redis_cart[sku_id])
            
        # ２.从mysql中获取对应商品信息
        skus = SKU.objects.filter(id__in=cart.keys())
        
        # 给查询处的商品对象添加count属性
        for sku in skus:
            sku.count = cart[sku.id]
        # 运费
        freight = Decimal('10.00')
        
        serializer = OrderSettlementSerializer({'freight':freight, 'skus':skus})
        return Response(serializer.data)

            
                
        
    