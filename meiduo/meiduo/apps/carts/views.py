import base64
import pickle

# Create your views here.
from django_redis import get_redis_connection
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from carts import constants
from carts.serialziers import CartSerializer, CartSKUSerializer, CartDeleteSerializer, CartSelectedSerializer
from goods.models import SKU


class CartView(APIView):
    def perform_authentication(self, request):
        """
        重写父类的用户验证方法，不在进入视图前就检查JWT
        """
        pass
    
    # 添加购物车记录
    def post(self,request):
        
        serializer = CartSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        sku_id = serializer.validated_data['sku_id']
        count = serializer.validated_data['count']
        selected = serializer.validated_data['selected']
        
        # 判断用户是否登陆
        try:
            user = request.user
        except Exception as e:
            user = None
        # 用户已登陆
        if user and user.is_authenticated:
            user_id = user.id
            redis_con = get_redis_connection('cart')
            pl = redis_con.pipeline()
            
            # 保存购物车中商品及数量
            pl.hincrby('cart_%s' % user_id,sku_id,count)
            
            # 保存购物车中商品的选中状态
            if selected:
                pl.sadd('cart_selected_%s' % user_id, selected)
            
            pl.execute()
            
            # 返回应答
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            cookie_cart = request.COOKIES.get('cart')
            if cookie_cart:
                cart_dict = pickle.loads(base64.b64decode(cookie_cart))
            else:
                cart_dict = {}
            
            if sku_id in cart_dict:
                count += cart_dict[sku_id]['count']

            cart_dict[sku_id] = {
                'count': count,
                'selected': selected
            }
            # 对cart_dict数据进行处理
            cart_data = base64.b64encode(pickle.dumps(cart_dict)).decode()
            
            response = Response(serializer.data, status=status.HTTP_201_CREATED)
            response.set_cookie('cart', cart_data, max_age=constants.CART_COOKIE_EXPIRES)
            
            return response

    # 获取购物车记录
    def get(self, request):

        # 获取用户信息
        try:
            user = request.user
        except Exception as e:
            user = None
        
        # 判断用户是否登陆
        if user and user.is_authenticated():
            user_id = user.id
            redis_con = get_redis_connection('cart')
            # 获取
            redis_cart = redis_con.hgetall('cart_%s' % user_id)
            # 获取被选中的购物车中商品id
            redis_selected_cart = redis_con.smembers('cart_selected_%s' % user_id)
            # 组织数据 -->方便统一处理
            # {
            #     '<sku_id>': {
            #         'count': '<count>',
            #         'selected': '<selected>'
            #     },
            #     ...
            # }
            cart_dict = {}
            for sku_id, count in redis_cart.items():
                cart_dict[int(sku_id)] = {
                    'count': int(count),
                    'selected': sku_id in redis_selected_cart
                }
                
        # 用户未登陆
        else:
            cookie_cart = request.COOKIES.get('cart')
            
            if cookie_cart:
                cart_dict = pickle.loads(base64.b64decode(cookie_cart))
            else:
                cart_dict = {}
        # 根据购物车记录获取对应的商品信息
        skus = SKU.objects.filter(id__in=cart_dict.keys())
        for sku in skus:
            sku.count = cart_dict[sku.id]['count']
            sku.selected = cart_dict[sku.id]['selected']
        serializer = CartSKUSerializer(skus, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    # 修改购物车信息
    def put(self, request):
   
        # 1接收并验证参数
        serializer = CartSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        sku_id = serializer.validated_data['sku_id']
        count = serializer.validated_data['count']
        selected = serializer.validated_data['selected']
        
        # 2判断用户是否登陆
        try:
            user = request.user
        except Exception as e:
            user = None
        #2.1用户登录状态下修改redis中购物车记录
        if user is not None and user.is_authenticated:
            user_id = user.id
            redis_con = get_redis_connection('cart')
            pl = redis_con.pipeline()
            # 设置用户购物车中商品的id和对应的count
            pl.hset('cart_%s' %user_id, sku_id, count)
            # 设置用户购物车勾选状态
            if selected:
                pl.sadd('cart_selected_%s' %user_id, sku_id)
            else:
                pl.srem('cart_selected_%s' %user_id, sku_id)
            pl.execute()
            return Response(serializer.data)
            
        #2.2用户登录状态下修改cookie中购物车记录
        else:
            response = Response(serializer.data)
            cookie_cart = request.COOKIES.get('cart')
            if cookie_cart is None:
                return response
            # 解析购物车记录
            cart_dict = pickle.loads(base64.b64decode(cookie_cart))
            
            if not cart_dict:
                return response
            
            cart_dict[sku_id] ={
                'count':count,
                'selected':selected
            }
            cookie_data = base64.b64encode(pickle._dumps(cart_dict)).decode()
            response.set_cookie('cart', cookie_data, max_age=constants.CART_COOKIE_EXPIRES)
            return response
                
    # 删除购物车信息
    def delete(self,request):
        # １获取并校验参数
        serializer = CartDeleteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sku_id = serializer.validated_data['sku_id']

    #     # ２判断用户是否登陆
        try:
            user = request.user
        except Exception as e:
            user = None
        #2.1如果用户已登陆,根据参数删除redis中对应的购物车记录
        if user is not None and user.is_authenticated():
            user_id = user.id
            redis_con = get_redis_connection('cart')
            pl = redis_con.pipeline()
            pl.hdel('cart_%s' %user_id, sku_id)
            pl.srem('cart_selected_%s' %user_id, sku_id)
            pl.execute()
            return Response(status=status.HTTP_204_NO_CONTENT)
        #2.2如果用户未登陆,根据参数删除对应的购物车记录
        else:
            response = Response(status=status.HTTP_204_NO_CONTENT)
            cart_cookie = request.COOKIES.get('cart')
            if cart_cookie is None:
                return response
            cart_dict = pickle.loads(base64.b64decode(cart_cookie))
            if not cart_dict:
                return response
            if sku_id in cart_dict:
                del cart_dict[sku_id]
            cookie_data = base64.b64encode(pickle._dumps(cart_dict)).decode()
            response = Response(status=status.HTTP_204_NO_CONTENT)
            response.set_cookie('cart', cookie_data, max_age=constants.CART_COOKIE_EXPIRES)
            return response

# 全选购物车记录
class CartAllView(APIView):
    """购物车全选"""
    def perform_authentication(self, request):
        """重写父类的用户验证方法，不在进入视图前就检查JWT"""
        pass
    
    def put(self,request):
        # 1.获取并校验参数
        serializer = CartSelectedSerializer(data=request.data)
        serializer.is_valid()
        selected = serializer.validated_data['selected']
        # 2.判断用户是否登陆
        try:
            user = request.user
        except Exception as e:
            user = None
            
        if user is not None and user.is_authenticated:
        # 2.1用户登录，修改redis中所有的商品状态修改为所传参数的状态
            user_id = user.id
            redis_con = get_redis_connection('cart')
            # 获取用户购物车中所有商品id
            sku_ids = redis_con.hkeys('cart_%s' % user_id)
            print(sku_ids)
            if selected:
                redis_con.sadd('cart_selected_%s' % user_id, *sku_ids)
            else:
                redis_con.srem('cart_selected_%s' % user_id, *sku_ids)
            return Response(status=status.HTTP_200_OK)
        else:
        # 2.2用户未登陆,获取客户端的cookie修改状态并返回
            cart_cookie = request.COOKIES.get('cart')
            if not cart_cookie:
                return Response
            cart_dict = pickle.loads(base64.b64decode(cart_cookie))
            if not cart_dict:
                return Response
            for sku_id in cart_dict:
                cart_dict[sku_id]['selected'] = selected
            # 构造cookie并返回
            cookie_data = base64.b64encode(pickle.dumps(cart_dict)).decode()
            response = Response()
            response.set_cookie('cart', cookie_data, max_age=constants.CART_COOKIE_EXPIRES)
            return response
                
            

