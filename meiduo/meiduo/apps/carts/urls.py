from django.conf.urls import url

from carts import views

urlpatterns = [
    url(r'^cart/$',views.CartView.as_view()),
    url(r'^cart/selection/$', views.CartAllView.as_view())
]