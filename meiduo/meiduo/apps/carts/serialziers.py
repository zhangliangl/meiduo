from rest_framework import serializers

from goods.models import SKU


class CartSerializer(serializers.Serializer):
    """购物车数据序列化"""
    sku_id = serializers.IntegerField(label='商品id', min_value=1)
    count = serializers.IntegerField(label='数量', min_value=1)
    selected = serializers.BooleanField(label='是否勾选', default=True)
    
    def validate(self, attrs):
        sku_id = attrs['sku_id']
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotEixt:
            raise serializers.ValidationError('商品不存在')
        
        if attrs['count'] > sku.stock:
            raise serializers.ValidationError('商品库存不足')
        return attrs
    

class CartSKUSerializer(serializers.ModelSerializer):
    """购物车商品数据序列化"""
    count = serializers.IntegerField(label='数量',min_value=1)
    selected = serializers.BooleanField(label='是否勾选', default=True)
    
    class Meta:
        model = SKU
        fields = ('id', 'count', 'name', 'default_image_url', 'price', 'selected')
        
class CartDeleteSerializer(serializers.Serializer):
    sku_id = serializers.IntegerField(label='商品编号', min_value=1)

    def validate(self, attrs):
        sku_id = attrs['sku_id']
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotEixt:
            raise serializers.ValidationError('商品不存在')
        return attrs
    
class CartSelectedSerializer(serializers.Serializer):
    selected = serializers.BooleanField(label='商品勾选状态')