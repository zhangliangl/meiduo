import pickle
import base64
from django_redis import get_redis_connection


def merge_cart_cookie_to_redis(request, user, response):
    # 获取cookie中的数据
    cart_cookie = request.COOKIES.get('cart')
    if not cart_cookie:
        return response
    cart_cookie_dict = pickle.loads(base64.b64decode(cart_cookie))
    print(cart_cookie_dict)
    if not cart_cookie_dict:
        return response
    
    # cookie中记录保存格式
    # {
    #     sku_id:{
    # count:count,
    # selected:selected
    # }
    # }
    # redis中保存格式
    # user_id:{
     #  sku_id:count
    # }
    # selected:
    # user_id{sku_id,...}
    # 用于保存向redis购物车商品数量hash添加数据的字典
    cart = {}

    # 记录redis勾选状态中应该增加的sku_id
    redis_cart_selected_add = []

    # 记录redis勾选状态中应该删除的sku_id
    redis_cart_selected_remove = []
    for sku_id, count_selected_dict in cart_cookie_dict.items():
        cart[sku_id] = count_selected_dict['count']
        
        if count_selected_dict['selected']:
            redis_cart_selected_add.append(sku_id)
        else:
            redis_cart_selected_remove.append(sku_id)
    if cart:
        print(cart)
        # 获取redis中的数据
        user_id = user.id
        redis_con = get_redis_connection('cart')
        pl = redis_con.pipeline()
        # 将cookie中商品数量同步到redis中
        pl.hmset('cart_%s' % user_id, cart)
        if redis_cart_selected_add:
            pl.sadd('cart_selected_%s' % user_id, *redis_cart_selected_add)
        if redis_cart_selected_remove:
            pl.srem('cart_selected_%s' % user_id, *redis_cart_selected_remove)
        pl.execute()
        
        response.delete_cookie('cart')
        return response
        
        
    