import django
import os




# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "meiduo.")# project_name 项目名称
# django.setup()

import random
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django_redis import get_redis_connection

import logging
logger = logging.getLogger('django')





# url('^/sms_codes/(?P<mobile>1[3-9]\d{9})/$',views.SMSCodeView.as_view())
from verifications import constants


class SMSCodeView(APIView):
    """
    短信验证码
    传入参数：
        mobile
    """
    def get(self, request,mobile):
        """发送短信验证码"""
       # 判断短息验证是否在６０秒内
        redis_coon = get_redis_connection('verify_codes')
        send_flag = redis_coon.get('send_flag_%s' %mobile)
        
        if send_flag:
            return Response({"message": "请求次数过于频繁"},status=status.HTTP_400_BAD_REQUEST)

       # 生成短息验证码
        sms_code = "%06d" %random.randint(0,999999)
        print('短息验证码：%s' %sms_code)
        # 短信验证码时间
        expires = constants.SMS_CODE_REDIS_EXPIRES//60
        # 发送短息验证码
        
        from celery_tasks.sms.tasks import send_sms_code
        # 异步发送短信
        # send_sms_code.delay(mobile,sms_code,expires)
        send_sms_code(mobile,sms_code,expires)
        
        # try:
        #     ccp = CCP().send_template_sms(mobile, [sms_code, expires], constants.SMS_CODE_TEMP_ID)
        # except Exception as e:
        #     logger.error(e)
        #     return Response({'message':'短息发送异常'}, status=status.HTTP_503_SERVICE_UNAVAILABLE)
        # if not ccp == 0:
        #
        #     return Response({'message':'短息发送失败'}, status=status.HTTP_503_SERVICE_UNAVAILABLE)

        # 保存短息验证码
        # question:redis一次只能执行一条命令
        # solve
        #   redis管道：可以向redis管道中添加多个redis命令，然后一次性执行
        p1 = redis_coon.pipeline()
        # 添加所有要执行的命令
        
        p1.setex('sms_code_%s' %mobile,constants.SMS_CODE_REDIS_EXPIRES,sms_code)



        # 保存短信验证码的标记
        p1.setex('send_flag_%s' %mobile,constants.SEND_SMS_CODE_INTERVAL,1)

        # 一次性执行管道中所有命令
        p1.execute()
        return Response({'message': '短息发送成功'}, status=status.HTTP_200_OK)
    
    